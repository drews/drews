# Drew Stinnett 👋

- 🔭 I’m currently working over in OIT
- 😄 Pronouns: he/him/his
- ⚡ Fun fact: Can you spot me in [this](https://www.youtube.com/watch?v=oL9WnB0qHBA)?

##   Recently Created Projects


##   Recent MRs


## 🍿 Recently Watched Films

- Watched [I'm Still Here, 2024 - ★★★★½](https://letterboxd.com/mondodrew/film/im-still-here-2024/) 2 days ago
- Watched [The Coffee Table, 2022 - ★★★½](https://letterboxd.com/mondodrew/film/the-coffee-table/) 3 days ago
- Watched [Riff Raff, 2024 - ★½](https://letterboxd.com/mondodrew/film/riff-raff-2024/) 3 days ago
- Watched [Elevation, 2024 - ★½](https://letterboxd.com/mondodrew/film/elevation/) 4 days ago
- Watched [Love Hurts, 2025 - ★★½](https://letterboxd.com/mondodrew/film/love-hurts-2025/) 6 days ago

## 📚 Recently Read Books

- [Mr. Mercedes (Bill Hodges Trilogy, #1)](https://www.goodreads.com/review/show/4982311822?utm_medium=api&utm_source=rss) by Stephen        King 2 years ago
- [Killing and Dying: Stories](https://www.goodreads.com/review/show/4882229933?utm_medium=api&utm_source=rss) by Adrian Tomine 2 years ago
- [The Drive-In 3: The Bus Tour](https://www.goodreads.com/review/show/4882229154?utm_medium=api&utm_source=rss) by Joe R. Lansdale 2 years ago
- [The Drive-in 2](https://www.goodreads.com/review/show/4882229065?utm_medium=api&utm_source=rss) by Joe R. Lansdale 2 years ago
- [Joe R. Lansdale's The Drive-In](https://www.goodreads.com/review/show/4882227635?utm_medium=api&utm_source=rss) by Joe R. Lansdale 2 years ago
