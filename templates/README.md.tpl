# Drew Stinnett 👋

- 🔭 I’m currently working over in OIT
- 😄 Pronouns: he/him/his
- ⚡ Fun fact: Can you spot me in [this](https://www.youtube.com/watch?v=oL9WnB0qHBA)?

##   Recently Created Projects
{{ range gitlabRecentlyCreatedProjects 5 }}
- [{{ .Name }}]({{ .WebURL }}) {{ .CreatedAt | builtinAgo }}
{{- end }}

##   Recent MRs
{{ range gitlabRecentlyAcceptedMergeRequests 5 }}
- {{ .Event.TargetTitle }} for [{{ .Project.Name }}]({{ .Project.WebURL }})
{{- end }}

## 🍿 Recently Watched Films
{{ range letterboxdRecentlyWatched 5 }}
- {{ .Verb }} [{{ .TitleWithRating }}]({{ .Link }}) {{ .PubDate | builtinAgo }}
{{- end }}

## 📚 Recently Read Books
{{ range goodreadsRecentlyRead 5 }}
- [{{ .Title }}]({{ .Link }}) by {{ .Author }} {{ .PubDate | builtinAgo }}
{{- end }}
